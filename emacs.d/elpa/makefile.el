;;; makefile.el --- Make Makefile                    -*- lexical-binding: t; -*-

;; Copyright (C) 2015  

;; Author:  <armita_a@pc-armita_a>
;; Keywords: 

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:


(provide 'makefile)

(eval-after-load 'autoinsert
  '(define-auto-insert '("\\Makefile\\'" . "Makefile")
     '(
       \n
       (my-std-file-header) \n
       (setq type (read-from-minibuffer (format "Type project (c/c++) : ")))
       (make-local-variable 'type)
       (kill-whole-line)
       
       (if (string= type "c")
	   "NAME	= "
	 "NAME		= ")
	> _ \n \n
	(if (string= type "c")
	    "SRC_DIR	= src/"
	  "SRC_DIR	= src/")
	\n \n
	(if (string= type "c")
	    "SRC	= "
	  "SRC		= ")
	\n \n	
       (if (string= type "c")
	   "OBJ	= $(SRC:.c=.o)"
       "OBJ		= $(SRC:.cpp=.o)")
       \n \n

       (if (string= type "c")
	   "CFLAGS	= -Wall -Wextra -W -pedantic -Iincludes"
	 "CXXFLAGS	= -Wall -Wextra -W -pedantic -Iincludes")
	 \n \n
	 "$(NAME):$(OBJ)" \n
	        (if (string= type "c")
		    "	$(CC) $(OBJ) -o $(NAME)"
		  "	$(CXX) $(OBJ) -o $(NAME)")
		\n
       "	@echo -e \"\033[032m$(NAME) compiled successfully\033[0m\"" \n \n
       "all:	$(NAME)" \n \n
       "clean:" \n
       "	rm -f $(OBJ)" \n \n
       "fclean:	clean" \n
       "	rm -f $(NAME)" \n \n
       "re:	fclean all" \n \n
       ".PHONY:	all clean fclean re" \n
       ;; dir-locales
       (write-region (concat "((nil . (
(company-clang-arguments . (\"-I" (locate-dominating-file buffer-file-name ".dir-locals.el") "/includes/\"))
(flycheck-clang-include-path . (" (locate-dominating-file buffer-file-name ".dir-locals.el") "/includes/\"))
)))\n") nil ".dir-locals.el")
       (write-region "(provide '.dir-locals)\n" nil ".dir-locals.el" 'append)
       ;; gitignore
       (write-region "# Created by https://www.gitignore.io

### Emacs ###
# -*- mode: gitignore; -*-
*~
\#*\#
/.emacs.desktop
/.emacs.desktop.lock
*.elc
auto-save-list
tramp
.\#*

# Org-mode
.org-id-locations
*_archive

# flymake-mode
*_flymake.*

# eshell files
/eshell/history
/eshell/lastdir

# elpa packages
/elpa/

# reftex files
*.rel

# AUCTeX auto folder
/auto/

# cask packages
.cask/


### C++ ###
# Compiled Object files
*.slo
*.lo
*.o
*.obj

# Precompiled Headers
*.gch
*.pch

# Compiled Dynamic libraries
*.so
*.dylib
*.dll

# Fortran module files
*.mod

# Compiled Static libraries
*.lai
*.la
*.a
*.lib

# Executables
*.exe
*.out
*.app


### C ###
# Object files
*.o
*.ko
*.obj
*.elf

# Precompiled Headers
*.gch
*.pch

# Libraries
*.lib
*.a
*.la
*.lo

# Shared objects (inc. Windows DLLs)
*.dll
*.so
*.so.*
*.dylib

# Executables
*.exe
*.out
*.app
*.i*86
*.x86_64
*.hex


### Python ###
# Byte-compiled / optimized / DLL files
__pycache__/
*.py[cod]

# C extensions
*.so

# Distribution / packaging
.Python
env/
build/
develop-eggs/
dist/
downloads/
eggs/
.eggs/
lib/
lib64/
parts/
sdist/
var/
*.egg-info/
.installed.cfg
*.egg

# PyInstaller
#  Usually these files are written by a python script from a template
#  before PyInstaller builds the exe, so as to inject date/other infos into it.
*.manifest
*.spec

# Installer logs
pip-log.txt
pip-delete-this-directory.txt

# Unit test / coverage reports
htmlcov/
.tox/
.coverage
.cache
nosetests.xml
coverage.xml

# Translations
*.mo
*.pot

# Django stuff:
*.log

# Sphinx documentation
docs/_build/

# PyBuilder
target/

.dir-locals.el
" nil ".gitignore"))))

;;; makefile.el ends here
