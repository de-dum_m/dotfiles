# Path to your oh-my-zsh configuration.
ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="agnoster"

#export WORKON_HOME=~/.virtualenvs
#source /usr/bin/virtualenvwrapper.sh

# if [ "$TERM" != "dumb" ] && [ -x /usr/bin/dircolors ]; then
#     eval "`dircolors ~/.config/color_scheme`"
# fi

# export PATH='/usr/share/perl5/site_perl/auto/share/dist/Cope:/usr/site/sbin:/usr/site/bin'
# Package
export PATH="${PATH}:/usr/local/sbin:/usr/local/bin"
# System
export PATH="${PATH}:/usr/sbin:/usr/bin:/sbin:/bin:$HOME/bin"
export PATH="${PATH}:$HOME/.gem/ruby/2.2.0/bin"
export PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"

# export WORKON_HOME="~/code/"
# source /usr/bin/virtualenvwrapper.sh

export GCC_COLORS="error=01;31:warning=01;33:note=01;35:caret=01;33:locus=01;34:quote=00;34"
export EDITOR='emacs -nw'
export HISTFILE="$HOME/.history"
export LOGCHECK='60'
export MAILCHECK=0
export PAGER='less'
export WATCH='all'
export WATCHFMT='%n has m %m at %T'

#export PYTHONSTARTUP=~/.pythonrc

export LESS_TERMCAP_mb=$'\E[01;31m'       # begin blinking
export LESS_TERMCAP_md=$'\E[01;38;5;74m'  # begin bold
export LESS_TERMCAP_me=$'\E[0m'           # end mode
export LESS_TERMCAP_se=$'\E[0m'           # end standout-mode
export LESS_TERMCAP_so=$'\E[38;5;246m'    # begin standout-mode - info box
export LESS_TERMCAP_ue=$'\E[0m'           # end underline
export LESS_TERMCAP_us=$'\E[04;38;5;146m' # begin underline


export JAVA_HOME='/usr/lib/jvm/java-8-jdk'

zstyle ':completion:*:*:ne:*:*files' ignored-patterns '*.o'

# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# -> Prevents accidentally clobbering files.
alias mkdir='mkdir -p'
mcd() {mkdir -p ./$* && cd ./$*}
alias xclip='xclip -selection c'

alias h='history'
alias j='jobs -l'
alias which='type -a'
alias gitinspection='gitinspector --grading -T -w --format=html -l > gitstats.html'

# Piping aliases
alias -g G='| grep'
alias -g L='| less'
alias -g PASTE='| curl -F "sprunge=<-" http://sprunge.us'

# Pretty-print of some PATH variables:
alias path='echo -e ${PATH//:/\\n}'
alias libpath='echo -e ${LD_LIBRARY_PATH//:/\\n}'
alias du='du -kh'    # Makes a more readable output.
alias df='df -kTh'

#-------------------------------------------------------------
# The 'ls' family (this assumes you use a recent GNU ls).
#-------------------------------------------------------------
# Add colors for filetype and  human-readable sizes by default on 'ls':
alias ls='ls -h --color'
alias lx='ls -lXB'         #  Sort by extension.
alias lk='ls -lSr'         #  Sort by size, biggest last.
alias lt='ls -ltr'         #  Sort by date, most recent last.
alias lc='ls -ltcr'        #  Sort by/show change time,most recent last.
alias lu='ls -ltur'        #  Sort by/show access time,most recent last.
alias l='ls'

# The ubiquitous 'll': directories first, with alphanumeric sorting:
alias ll="ls -lv --group-directories-first"
alias lm='ll |more'        #  Pipe through 'more'
alias lr='ll -R'           #  Recursive ls.
alias la='ll -A'           #  Show hidden files.
alias tree='tree -Csuh'    #  Nice alternative to 'recursive ls' ...

alias ping='ping -c2'
alias pong='ping -c2 google.com && sleep 1 && clear'

# Ssh serveur
alias 4242='mosh --ssh="ssh -p 2222" maxime@4242.co' # -p 60001 || mosh --ssh="ssh -p 2222" maxime@4242.co -p 60002'
alias irc='mosh --ssh="ssh -p 2222" maxime@4242.co -- screen -dr irssi'

function wip()
{
    ip addr show wlo1 | grep inet | awk -F'/' '{ print $1 }' | grep -v inet6 | awk '{ print $2}'
}

function swap()
{ # Swap 2 filenames around, if they exist (from Uzi's bashrc).
    local TMPFILE=tmp.$$

    [ $# -ne 2 ] && echo "swap: 2 arguments needed" && return 1
	        [ ! -e $1 ] && echo "swap: $1 does not exist" && return 1
		[ ! -e $2 ] && echo "swap: $2 does not exist" && return 1

		mv "$1" $TMPFILE
		mv "$2" "$1"
		mv $TMPFILE "$2"
}

function extract()      # Handy Extract Program
{
    if [ -f $1 ] ; then
	case $1 in
	    *.tar.bz2)   tar xvjf $1     ;;
	    *.tar.gz)    tar xvzf $1     ;;
	    *.bz2)       bunzip2 $1      ;;
	    *.rar)       unrar x $1      ;;
	    *.gz)        gunzip $1       ;;
	    *.tar)       tar xvf $1      ;;
	    *.tbz2)      tar xvjf $1     ;;
	    *.tgz)       tar xvzf $1     ;;
	    *.zip)       unzip $1        ;;
	    *.Z)         uncompress $1   ;;
	    *.7z)        7z x $1         ;;
	    *)           echo "'$1' cannot be extracted via >extract<" ;;
	esac
    else
	echo "'$1' is not a valid file!"
    fi
}

# Creates an archive (*.tar.gz) from given directory.
function maketar() { tar cvzf "${1%%/}.tar.gz"  "${1%%/}/"; }

# Create a ZIP archive of a file or folder.
function makezip() { zip -r "${1%%/}.zip" "$1" ; }

function my_ps() { ps $@ -u $USER -o pid,%cpu,%mem,bsdtime,command ; }
function pp() { my_ps f | awk '!/awk/ && $0~var' var=${1:-".*"} ; }

## Spawn from CWD in urxvt

cwd_to_urxvt() {
    local update="\0033]777;cwd-spawn;path;$PWD\0007"

    case $TERM in
	screen*)
	    # pass through to parent terminal emulator
	    update="\0033P$update\0033\\";;
    esac

    echo -ne "$update" >/dev/null
}

cwd_to_urxvt # execute upon startup to set initial directory

ssh_connection_to_urxvt() {
    # don't propagate information to urxvt if ssh is used non-interactive
    [ -t 0 ] || [ -t 1 ] || return

    local update="\0033]777;cwd-spawn;ssh;$1\0007"

    case $TERM in
	screen*)
	    # pass through to parent terminal emulator
	    update="\0033P$update\0033\\";;
    esac

    echo -ne "$update"
}

chpwd_functions=(${chpwd_functions} cwd_to_urxvt)

function killps()   # kill by process name
{
        local pid pname sig="-TERM"   # default signal
	if [ "$#" -lt 1 ] || [ "$#" -gt 2 ]; then
	    echo "Usage: killps [-SIGNAL] pattern"
	    return;
	fi
	if [ $# = 2 ]; then sig=$1 ; fi
	for pid in $(my_ps| awk '!/awk/ && $0~pat { print $1 }' pat=${!#} )
	do
	    pname=$(my_ps | awk '$1~var { print $5 }' var=$pid )
	    if ask "Kill process $pid <$pname> with signal $sig?"
	    then kill $sig $pid
	    fi
	done
}

function my_ip() # Get IP adress on ethernet.
{
    MY_IP=$(/sbin/ifconfig eth0 | awk '/inet/ { print $2 } ' || sed -e s/addr://)
    echo ${MY_IP:-"Not connected"}
}

setopt AUTO_CD
setopt ALWAYS_LAST_PROMPT
setopt ALWAYS_TO_END
setopt AUTO_MENU
setopt AUTO_PARAM_SLASH
setopt COMPLETE_ALIASES
setopt COMPLETE_IN_WORD
setopt HASH_LIST_ALL
setopt LIST_AMBIGUOUS
unsetopt LIST_BEEP
setopt LIST_TYPES
unsetopt ALL_EXPORT
setopt ALIASES
setopt CORRECT
unsetopt CORRECT_ALL
setopt NOCORRECTALL
setopt INTERACTIVE_COMMENTS
setopt HASH_CMDS
setopt HASH_DIRS
unsetopt MAIL_WARNING
setopt AUTO_CONTINUE
setopt AUTO_RESUME
setopt BG_NICE
unsetopt HUP
setopt LONG_LIST_JOBS
setopt TRANSIENT_RPROMPT
setopt C_BASES
setopt FUNCTION_ARGZERO
setopt BSD_ECHO
setopt BEEP
setopt EMACS
setopt ZLE


bindkey -e

google()
{
        local s="$_"
    local query=

    case "$1" in
        '')   ;;
        that) query="search?q=${s//[[:space:]]/+}" ;;
        *)    s="$*"; query="search?q=${s//[[:space:]]/+}" ;;
    esac

    nohup google-chrome "http://www.google.com/${query}" >> /dev/null &
}
alias error='python ~/Documents/scripts/tests/test.py . -nocheat -verbose -score -libc -return -printline -malloc'
clean()
{
    SEARCH='.'
    if [ ${1} ]
    then
	SEARCH=${1}
    fi
    find ${SEARCH} \( -name "*~" -or -name ".*~" -or -name "#%2Amerge%2A*#" -or -name "*.orig" -or -name "*_BACKUP_*" -or -name "*_BASE_*" -or -name "*_LOCAL_*" -or -name "*_REMOTE_*" -or -name "*.o" -or -name "#*#" \) -exec rm -fv {} \;
}

# Set to this to use case-sensitive completion
# CASE_SENSITIVE="true"

# Uncomment this to disable bi-weekly auto-update checks
# DISABLE_AUTO_UPDATE="true"

# Uncomment to change how often before auto-updates occur? (in days)
# export UPDATE_ZSH_DAYS=13

# Uncomment following line if you want to disable colors in ls
# DISABLE_LS_COLORS="true"

# Uncomment following line if you want to disable autosetting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment following line if you want to disable command autocorrection
# DISABLE_CORRECTION="true"

# Uncomment following line if you want red dots to be displayed while waiting for completion
#COMPLETION_WAITING_DOTS="true"

# Uncomment following line if you want to disable marking untracked files under
# VCS as dirty. This makes repository status check for large repositories much,
# much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
plugins=(git last-working-dir sprunge zsh-auto-suggestions zsh-syntax-highlighting history-substring-search zsh-iterm-touchbar)

autoload -U promptinit
autoload run-help-git
promptinit

source $ZSH/oh-my-zsh.sh

# Customize to your needs...
zstyle ':completion:*' list-colors "${(@s.:.)LS_COLORS}"
autoload -Uz compinit
compinit

neofetch || echo
echo

export GITLAB_API_ENDPOINT='https://gitlab.com/api/v3'
export GITLAB_API_PRIVATE_TOKEN='4RQbX3dc8e2C1Qi_56tx'

function powerline_precmd() {

    #export PS1="$(~/.powerline-shell.py $? --shell zsh 2> /dev/null)"
    export PROMPT="$(~/.powerline-shell.py $? --shell zsh 2> /dev/null)"
}

function install_powerline_precmd() {
    for s in "${precmd_functions[@]}"; do
	if [ "$s" = "powerline_precmd" ]; then
	    return
	fi
    done
    precmd_functions+=(powerline_precmd)
}

#install_powerline_precmd

AUTOSUGGESTION_HIGHLIGHT_COLOR=fg=11
AUTOSUGGESTION_ACCEPT_RIGHT_ARROW=1
source /usr/local/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
bindkey "^[[A" history-substring-search-up
bindkey "^[[B" history-substring-search-down