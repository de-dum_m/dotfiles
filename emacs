;; My emacs config

;;(load "tuareg-site-file.el")
(if (file-exists-p "~/.myemacs")
    (load-file "~/.myemacs"))
(add-to-list 'load-path "/home/de-dum_m/.emacs.d/lisp/")
(add-to-list 'load-path "/home/de-dum_m/.emacs.d/es-lib/")
(add-to-list 'load-path "/home/de-dum_m/.emacs.d/elpa/")
(add-to-list 'load-path "/home/de-dum_m/.emacs.d/es-windows/")
(add-to-list 'load-path "~/.emacs.d/el-get/el-get")

(require 'pyvenv)
(require 'header)
(require 'makefile)

(require 'package)
(add-to-list 'package-archives
	     '("melpa" . "http://melpa.org/packages/"))
(package-initialize)

(add-hook 'python-mode-hook 'anaconda-mode)
(autoload 'python-mode "python-mode" "Python Mode." t)
 (add-to-list 'auto-mode-alist '("\\.py\\'" . python-mode))
  (add-to-list 'interpreter-mode-alist '("python" . python-mode))

;; (unless (require 'el-get nil 'noerror)
;;   (require 'package)
;;   (add-to-list 'package-archives
;; 	       '("melpa" . "http://melpa.org/packages/"))
;;   (package-refresh-contents)
;;   (package-initialize)
;;   (package-install 'el-get)
;;   (require 'el-get))

;; (add-to-list 'el-get-recipe-path "~/.emacs.d/el-get-user/recipes")
;; (el-get 'sync)

(load-theme 'solarized t)

(put 'upcase-region 'disabled nil)

;; (require 'yasnippet)

(add-hook 'after-init-hook #'projectile-global-mode)
(require 'helm-projectile)
(helm-projectile-on)

(add-hook 'after-init-hook #'global-flycheck-mode)

(require 'flycheck-color-mode-line)

(eval-after-load "flycheck"
  '(add-hook 'flycheck-mode-hook 'flycheck-color-mode-line-mode))

(eval-after-load "flycheck"
  '(setq flycheck-check-syntax-automatically '(save mode-enabled new-line)))

(auto-insert-mode)
(setq auto-insert-query nil)

(setq auto-mode-alist (cons '("\.lua$" . lua-mode) auto-mode-alist))
    (autoload 'lua-mode "lua-mode" "Lua editing mode." t)

;; Suppression des espaces en fin de ligne a l'enregistrement
(add-hook 'c++-mode-hook '(lambda ()
			    (add-hook 'write-contents-hooks 'delete-trailing-whitespace nil t)))
(add-hook 'c-mode-hook '(lambda ()
			  (add-hook 'write-contents-hooks 'delete-trailing-whitespace nil t)))

;supprimer fichier de sauvegarde
(setq make-backup-files nil)


;; Suppression bar de menu
(menu-bar-mode -1)

;Coloriser au max
(require 'font-lock)
(global-font-lock-mode t)
(setq font-lock-maximum-decoration t)
(setq font-lock-maximum-size nil)

;; Affiche le numero de ligne et de colonne
(column-number-mode t)
(line-number-mode t)

;; Display line numbers
(global-linum-mode 1)
(setq linum-format "%4d ")

;; (set-face-attribute 'linum nil :foreground "#585858")
;; (set-face-attribute 'linum nil :background "#262626")

(require 'fill-column-indicator)
(setq-default fci-rule-column 80)
(setq-default fci-rule-color "blue")
(setq-default fci-rule-character 9474)

(require 'project-explorer)
(global-set-key (kbd "C-T") 'project-explorer-toggle)

(autoload 'company-mode "company" nil t)
(with-eval-after-load 'company
  (define-key company-active-map [tab] 'company-complete-common-or-cycle)
  (define-key company-active-map (kbd "TAB") 'company-complete-common-or-cycle))


(defun completions ()
  (yas-global-mode 1)
  (global-company-mode 1)
  (add-to-list 'company-backends 'company-c-headers))
;; (add-to-list 'company-c-headers-path-system "/usr/include/c++/4.8/")
(add-hook 'after-init-hook 'completions)

(defun check-expansion ()
  (save-excursion
    (if (looking-at "\\_>") t
      (backward-char 1)
      (if (looking-at "\\.") t
	(backward-char 1)
	(if (looking-at "->") t nil)))))

(defun do-yas-expand ()
  (let ((yas/fallback-behavior 'return-nil))
    (yas/expand)))

(setq ac-ignore-case nil)
;; C hooks
(add-hook 'c-mode-hook '(lambda ()
			   (show-paren-mode)
			   (fci-mode)))

(add-hook 'c++-mode-hook '(lambda ()
			   (show-paren-mode)
			   (fci-mode)))

(add-hook 'python-mode-hook '(lambda ()
			    ;; (add-hook 'python-mode-hook 'jedi:ac-setup)
			    ;; (setq jedi:server-command '("python" "/home/de-dum_m/.emacs.d/elpa/jedi-20150109.2230/jediepcserver.py"))
			    ;; (setq jedi:tooltip-method '(pos-tip))
			    ;; (setq jedi:complete-on-dot t)
			    ;; (setq jedi:setup-keys t)
			   (show-paren-mode)
			   (fci-mode)))


(font-lock-add-keywords 'c-mode
  '(("\\(\\w+\\)\\s-*\("
     (1 c-annotation-face)))
 t)

;; Enables copy/pasting.
(setq x-select-enable-clipboard t)
(require 'mouse)
(xterm-mouse-mode t)
(defun track-mouse (e))
(setq mouse-sel-mode t)
(require 'xclip)
(xclip-mode 1)

(defvar-local company-fci-mode-on-p nil)

;; Fix for fci-mode + company-mode
(defun company-turn-off-fci (&rest ignore)
  (when (boundp 'fci-mode)
    (setq company-fci-mode-on-p fci-mode)
    (when fci-mode (fci-mode -1))))

(defun company-maybe-turn-on-fci (&rest ignore)
  (when company-fci-mode-on-p (fci-mode 1)))

(defun tab-indent-or-complete ()
  (interactive)
  (if (minibufferp)
      (minibuffer-complete)
    (if (or (not yas/minor-mode)
	    (null (do-yas-expand)))
	(if (check-expansion)
	    (company-complete-common)
	  (indent-for-tab-command)))))
(global-set-key "\t" 'tab-indent-or-complete)

(add-hook 'company-completion-started-hook 'company-turn-off-fci)
(add-hook 'company-completion-finished-hook 'company-maybe-turn-on-fci)
(add-hook 'company-completion-cancelled-hook 'company-maybe-turn-on-fci)

(eval-after-load "company"
  '(progn
     (add-to-list 'company-backends '(company-anaconda :with company-dabbrev))))

(global-hl-line-mode t)
;;(custom-set-faces '(highlight ((t (:weight bold)))))

(setq ac-clang-flags
      (mapcar (lambda (item)(concat "-I" item))
	      (split-string
	                      "
/usr/lib/gcc/x86_64-unknown-linux-gnu/4.9.1/../../../../include/c++/4.9.1
 /usr/lib/gcc/x86_64-unknown-linux-gnu/4.9.1/../../../../include/c++/4.9.1/x86_64-unknown-linux-gnu
 /usr/lib/gcc/x86_64-unknown-linux-gnu/4.9.1/../../../../include/c++/4.9.1/backward
 /usr/lib/gcc/x86_64-unknown-linux-gnu/4.9.1/include
 /usr/local/include
 /usr/lib/gcc/x86_64-unknown-linux-gnu/4.9.1/include-fixed
 /usr/include
 /usr/include/qt
/usr/include/qt4

/usr/include/qt/QtCore
/usr/include/qt/QtWidgets
/usr/include/qt/QtGui
/usr/include/qt4/QtCore
/usr/include/qt4/QtGui
")))

;; (require 'color)

;;   (let ((bg (face-attribute 'default :background)))
;;     (custom-set-faces
;;      `(company-tooltip ((t (:inherit default :background ,(color-lighten-name bg 2)))))
;;      `(company-scrollbar-bg ((t (:background ,(color-lighten-name bg 10)))))
;;      `(company-scrollbar-fg ((t (:background ,(color-lighten-name bg 5)))))
;;      `(company-tooltip-selection ((t (:inherit font-lock-function-name-face))))
;;      `(company-tooltip-common ((t (:inherit font-lock-constant-face))))))


(global-set-key "\M-[1;5C"    'forward-word)  
(global-set-key "\M-[1;5D"    'backward-word) 
(global-set-key "\M-[1;5A"    'backward-paragraph)
(global-set-key "\M-[1;5B"    'forward-paragraph)
(windmove-default-keybindings 'meta)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(company-auto-complete (quote (quote company-explicit-action-p)))
 '(company-backends
   (quote
    ((company-anaconda :with company-dabbrev)
     company-clang company-c-headers company-predictive company-files company-elisp company-bbdb company-nxml company-css company-eclim company-semantic company-clang company-xcode company-ropemacs company-cmake company-capf
     (company-dabbrev-code company-gtags company-etags company-keywords)
     company-oddmuse company-files company-dabbrev)))
 '(custom-theme-load-path
   (quote
    ("/home/de-dum_m/.emacs.d/themes/emacs-color-theme-solarized/" custom-theme-directory t)))
 '(delete-selection-mode nil)
 '(flycheck-clang-include-path
   (quote
    ("/home/de-dum_m/code/cpp/bomberman/srcs/includes/")))
 '(mark-even-if-inactive t)
 '(safe-local-variable-values
   (quote
    ((company-clang-arguments "-I/home/de-dum_m/code/sys_unix/zappy/includes/" "-I/home/de-dum_m/code/sys_unix/zappy/includes/server/" "-I/home/de-dum_m/code/sys_unix/zappy/includes/client/" "-I/home/de-dum_m/code/sys_unix/zappy/includes/display/")
     (flycheck-include-path "/home/de-dum_m/code/cpp/bomberman/includes/Graph/" "/home/de-dum_m/code/cpp/bomberman/includes/Core/" "/home/de-dum_m/code/cpp/bomberman/LibBomberman_linux_x64/includes/")
     (company-clang-arguments "-I/home/de-dum_m/code/cpp/bomberman/includes/Graph/" "-I/home/de-dum_m/code/cpp/bomberman/includes/Core/" "-I/home/de-dum_m/code/cpp/bomberman/LibBomberman_linux_x64/includes/" "-std=c++11")
     (flycheck-include-path "/home/de-dum_m/code/cpp/bomberman/srcs/includes/" "/home/de-dum_m/code/cpp/bomberman/srcs/includesGraph/" "/home/de-dum_m/code/cpp/bomberman/srcs/libGraph/LibBomberman_linux_x64/includes/")
     (company-clang-arguments "-I/home/de-dum_m/code/cpp/bomberman/srcs/includes/" "-I/home/de-dum_m/code/cpp/bomberman/srcs/includesGraph/" "-I/home/de-dum_m/code/cpp/bomberman/srcs/libGraph/LibBomberman_linux_x64/includes/" "-std=c++11")
     (flycheck-include-path "/home/de-dum_m/code/cpp/bomberman/srcs/includes/" "/home/de-dum_m/code/cpp/bomberman/srcs/includesGraph/")
     (company-clang-arguments "-I/home/de-dum_m/code/cpp/bomberman/srcs/includes/" "-I/home/de-dum_m/code/cpp/bomberman/srcs/includesGraph/" "-std=c++11")
     (flycheck-clang-language-standard "c++11")
     (flycheck-include-path "/home/de-dum_m/code/cpp/bomberman/srcs/includes/")
     (company-clang-arguments "-I/home/de-dum_m/code/cpp/bomberman/srcs/includes/" "-std=c++11")
     (company-clang-arguments "-I/home/de-dum_m/code/cpp/plazza/sources/includes" "-I/usr/include/qt4/QtCore" "-I/usr/include/qt4/QtGui")
     (company-clang-arguments "/home/de-dum_m/code/cpp/plazza/sources/includes" "/usr/include/qt4/QtCore" "/usr/include/qt4/QtGui")
     (company-clang-arguments "/home/de-dum_m/code/cpp/plazza/sources/includes" "-pipe -O2 -march=x86-64 -mtune=generic -O2 -pipe -fstack-protector-strong --param=ssp-buffer-size=4 -D_REENTRANT -Wall -W -fPIE -DQT_NO_DEBUG -DQT_DECLARATIVE_LIB -DQT_WIDGETS_LIB -DQT_GUI_LIB -DQT_SCRIPT_LIB -DQT_CORE_LIB -I. -I. -Iincludes -isystem /usr/include/qt -isystem /usr/include/qt/QtDeclarative -isystem /usr/include/qt/QtWidgets -isystem /usr/include/qt/QtGui -isystem /usr/include/qt/QtScript -isystem /usr/include/qt/QtCore -I. -I. -I/usr/lib/qt/mkspecs/linux-g+")
     (company-clang-arguments "-I/home/de-dum_m/code/cpp/plazza/sources/includes/" "-pipe -O2 -march=x86-64 -mtune=generic -O2 -pipe -fstack-protector-strong --param=ssp-buffer-size=4 -D_REENTRANT -Wall -W -fPIE -DQT_NO_DEBUG -DQT_DECLARATIVE_LIB -DQT_WIDGETS_LIB -DQT_GUI_LIB -DQT_SCRIPT_LIB -DQT_CORE_LIB -I. -I. -Iincludes -isystem /usr/include/qt -isystem /usr/include/qt/QtDeclarative -isystem /usr/include/qt/QtWidgets -isystem /usr/include/qt/QtGui -isystem /usr/include/qt/QtScript -isystem /usr/include/qt/QtCore -I. -I. -I/usr/lib/qt/mkspecs/linux-g+")
     (company-clang-arguments "-I/home/de-dum_m/code/cpp/plazza/includes/")
     (company-clang-arguments "-I/home/de-dum_m/code/sys_unix/psu_2014_myirc/includes/")
     (company-clang-arguments . "-I/home/de-dum_m/code/cpp/cpp_nibbler/includes/")
     (company-clang-arguments . "/home/de-dum_m/code/sys_unix/psu_2014_myirc/includes/")
     (company-clang-arguments . "-I/home/de-dum_m/code/sys_unix/psu_2014_myirc/includes")
     (company-clang-arguments . -I/home/de-dum_m/code/sys_unix/psu_2014_myirc/includes/)
     (company-clang-arguments . "-I/home/de-dum_m/code/sys_unix/psu_2014_myirc/includes/")
     (company-clang-arguments . -I/home/de-dum_m/code/cpp/cpp_nibbler/includes/)
     (company-clang-arguments "-I/home/de-dum_m/code/cpp/cpp_nibbler/includes/" "-I/home/de-dum_m/code/cpp/cpp_nibbler/libs/ncurses/includes/" "-lcurses")
     (company-clang-arguments "-I/home/de-dum_m/code/cpp/cpp_nibbler/includes/" "-I/home/de-dum_m/code/cpp/cpp_nibbler/libs/ncurses/includes/")
     (company-clang-arguments "-I/home/de-dum_m/code/cpp/cpp_nibbler/includes/" "-I/home/de-dum_m/code/cpp/cpp_nibbler/libs/ncurses/includes")
     (company-clang-arguments "-I/home/de-dum_m/code/cpp/cpp_nibbler/includes/ -I/home/de-dum_m/code/cpp/cpp_nibbler/libs/ncurses/includes")
     (company-clang-arguments "-I/home/de-dum_m/code/sys_unix/psu_2014_myftp/includes/ -D_GNU_SOURCE")
     (company-clang-arguments "-I/home/de-dum_m/code/sys_unix/psu_2014_myftp/includes/")
     (company-clang-arguments "-I/home/de-dum_m/code/sys_unix/elcrypt/includes/")
     (company-clang-arguments "-I/home/de-dum_m/code/sys_unix/psu_2014_lemipc/includes/")
     (company-clang-arguments "-I/home/de-dum_m/includes/")
     (company-clang-arguments "-I/home/de-dum_m/code/sys_unix/psu_2014_philo/includes/")
     (company-clang-arguments "-I/home/de-dum_m/code/cpp/cpp_abstractvm/includes/")
     (company-clang-arguments "-I/home/de-dum_m/code/malloc/includes/")
     (company-clang-arguments "-I/home/de-dum_m/code/malloc/includes")
     (company-clang-arguments "-I/home/de-dum_m/code/piscine/rush_pokemon/pokedex/includes")
     (company-clang-arguments "-I/home/de-dum_m/code/piscine/rush_pokemon/includes")
     (company-clang-arguments "-I/home/de-dum_m/code/piscine/rush_santa/includes")
     (company-clang-arguments "-I /home/de-dum_m/code/piscine/rush_santa/includes")
     (company-clang-arguments "-Iincludes"))))
 '(scroll-bar-mode (quote right))
 '(sp-autoescape-string-quote nil)
 '(sp-autoescape-string-quote-if-empty (quote (c-mode c++-mode python-mode)))
 '(sp-navigate-close-if-unbalanced t)
 '(transient-mark-mode 1)
 '(vc-follow-symlinks t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
